# 1.Задача:
# Написать программу, которая запрашивает на ввод имя фамилию и возраст у пользователя. И должна вывести верны ли данные или нет.
# Правильный имя Иван фамилия Петров и возраст 32.
# Важно выполнить все проверки(в имени и фамилии не должно быть каких либо символов, кроме букв, в возрасте только цифры.Имя и фамилия должны
# быть обязательно с большой буквы.В случае неправильного ввода должно выдаваться сообщение о ошибке)

firstName=input("Введите имя: ")
secondName=input("Введите фамилию: ")
age=input("Введите ваш возрат: ")
if firstName.isalpha() and secondName.isalpha() and firstName.istitle() and secondName.istitle() and age.isdigit():
    print("Все верно")
elif not(firstName.isalpha()):
    print("Имя введено неверно, присутствуют не только буквы")
elif not(secondName.isalpha()):
    print("Фамилия введена неверно, присутствуют не только буквы")
elif not(firstName.istitle()):
    print("Имя введено неверно, заглавная буква маленькая")
elif not(secondName.istitle()):
    print("Фамилия введена неверно, заглавная буква маленькая")
elif not(age.isdigit()):
    print("Возраст введен неверно, присутствуют не только цифры")